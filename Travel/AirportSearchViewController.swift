import UIKit

/**
 `AirportSearchViewController` provides an interface for searching and selecting an airport. Upon selection the controller call's it's callback block `didSelectAirport` with the
 selection and then dismisses itself.
 */
public final class AirportSearchViewController: UITableViewController {
	
	// MARK: - Public
	/// Completion block called when user select's an airport from the tableview
	public var didSelectAirport: (Airport) -> Void = { _ in }
	
	
	// MARK: - Private
	
	private var airports = [Airport]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	
	// MARK: - Search
	
	@IBOutlet private var searchBar: UISearchBar?
	
	private lazy var searchingIndicator: UIActivityIndicatorView = {
		let searchingIndicator = UIActivityIndicatorView(style: .medium)
		searchingIndicator.hidesWhenStopped = true
		return searchingIndicator
	}()
	
	private var searchWorkItem: DispatchWorkItem?
	
	private func updateAirports() {
		let searchText: String? = self.searchText
		
		AirportDataSource.shared.fetchAirports(withSearchText: searchText) { (airports: [Airport]) in
			if searchText == self.searchText {
				self.airports = airports
				self.searchingIndicator.stopAnimating()
			}
		}
	}
	
	private static let searchDelay: TimeInterval = 0.75
	
	private var searchText: String? {
		didSet {
			if oldValue == self.searchText {
				return
			}
			
			self.searchingIndicator.startAnimating()
			
			self.searchWorkItem?.cancel()
			let searchWorkItem = DispatchWorkItem(){ [weak self] in
				self?.updateAirports()
			}
			self.searchWorkItem = searchWorkItem
			DispatchQueue.main.asyncAfter(deadline: .now() + AirportSearchViewController.searchDelay, execute: searchWorkItem)
		}
	}
	
	
    // MARK: - UIViewController
	
    public override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.searchingIndicator)

		self.updateAirports()
    }
	
	public override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.searchBar?.becomeFirstResponder()
	}
	
    // MARK: - UITableViewDataSource
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airports.count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search-id", for: indexPath)
        let airport = airports[indexPath.row]
        cell.textLabel?.text = airport.name
        cell.detailTextLabel?.text = airport.shortCode
        return cell
    }

    
	// MARK: - UITableViewDelegate

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectAirport(airports[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
	
}


// MARK: - UISearchBarDelegate

extension AirportSearchViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		self.searchText = searchText
    }
}
