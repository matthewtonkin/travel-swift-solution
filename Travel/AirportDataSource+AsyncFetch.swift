//
//  AirportDataSource+AsyncFetch.swift
//  Travel
//
//  Created by Matthew Tonkin on 5/11/19.
//  Copyright © 2019 Around. All rights reserved.
//

import Foundation

extension AirportDataSource {
	
	private static var searchQueue: DispatchQueue = DispatchQueue.global(qos: .default)
	
	func fetchAirports(withSearchText searchText: String?, completion: @escaping ([Airport]) -> Swift.Void) {
		if Thread.isMainThread {
			// Search with searchQueue and complete on main thread
			AirportDataSource.searchQueue.async {
				let airports: [Airport] = self.airportsMatching(searchText)
				DispatchQueue.main.async {
					completion(airports)
				}
			}
		} else {
			// We're not on the main thread, so search and complete on current thread
			let airports: [Airport] = self.airportsMatching(searchText)
			completion(airports)
		}
	}
	
}
