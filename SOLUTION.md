#  Travel App Take-Home Exercise - Solution

Matthew Tonkin - 11/5/19

## Major issues:

1. Primary issue is that `AirportSearchDataSource.swift` searches asynchronously, however `AirportSearchViewController.swift` expects it to be synchronous, thereby blocking the main thread when searches are executed.
Ideally this would be resolved by modifying the data source to expose a search function that is _implicity_ asynchronous.
`func fetchAirports(withSearchText searchText: String?, completion: @escaping ([Airport]) -> Swift.Void)`
This is a better function name as using the 'fetch' prefix and a completion handler heavily imply it's asynchronous, a programmer with any experience would implicity know that without any further documentation
Due to `AirportSearchDataSource.swift` being off limits for this excercise, this problem was solved by using a class extension `AirportDataSource+AsyncFetch.swift`.
The threading implementation in this extension always calls the completion on the same thread it was called on, therefore reducing the possibility for programmer error.

2. If searches are going to hit a server, then searching every key stroke would hit the server much harder than necessary. Searching therefore needs to be throttled.
This was implemented by using a `DispatchQueue` and `DispatchWorkItem` which is cancelled if the search term is updated in <0.75 seconds.


## Small UX improvements:
- The UI didn't indicate that a search was pending or active. A `UIActivityIndicatorView` was added programatically.
- Search Bar was not activated when the search UI is shown. Now on `viewDidLoad()` the search bar becomes first responder and is in editing. It's quicker to use, but also quicker to test.


## Minor issues:
- Gitignore should include .DS_Store, xcuserdata or a bunch of other trivial files that change often
- Project has no real structure, grouping by purpose (preferably) or type would improve navigation, especially as the project expands
- File names and class names don't match, making it more difficult for new programmers to get aquianted


## Future improvements:
- AirportSearchDataSource API should probably be
`func fetchAirports(withSearchText searchText: String?, completion: @escaping (Set<[Airport]>?, Error?) -> Swift.Void)`
This is because `Airport`s are unique and therefore would return a set rather than an array (which removes duplicates, but also makes the func clearer to the programmer), and any server interaction may result in an error, therefore the func would need to return an error which _could_ be displayed to the user if necessary
- Reachability - If the datasource requires an internet connection, then it's necessary to handle reachability / connection issues
- Unit testing - Test that the datasource actually works & responds within a usable period of time
- Delegate pattern for AirportSearchViewController - Current block based implementation is fine for now, however may not scale well
